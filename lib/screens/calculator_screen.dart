// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';

ButtonStyle secondaryStyle = ElevatedButton.styleFrom(
  primary: Colors.grey,
  onPrimary: Colors.black,
);
ButtonStyle operatorStyle = ElevatedButton.styleFrom(
  primary: Colors.amber,
  onPrimary: Colors.white,
);

class CalculatorScreen extends StatefulWidget {
  const CalculatorScreen({Key? key}) : super(key: key);

  @override
  State<CalculatorScreen> createState() => _CalculatorScreenState();
}

class _CalculatorScreenState extends State<CalculatorScreen> {
  String _inputValue = '0';
  String _calculatedValue = '';
  String _currentOperator = '';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: Column(
        children: [
          Expanded(
            flex: 2,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Text(
                  _calculatedValue,
                  textAlign: TextAlign.end,
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 50,
                  ),
                  overflow: TextOverflow.fade,
                  maxLines: 1,
                  softWrap: false,
                )
              ],
            ),
          ),
          Expanded(
            flex: 1,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Text(
                  _inputValue,
                  textAlign: TextAlign.end,
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 36,
                  ),
                  overflow: TextOverflow.fade,
                  maxLines: 1,
                  softWrap: false,
                )
              ],
            ),
          ),
          FirstRow(),
          SecondRow(),
          ThirdRow(),
          FourthRow(),
          FifthRow(),
        ],
      ),
    );
  }

  Expanded FifthRow() {
    return Expanded(
      flex: 1,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Expanded(
            flex: 2,
            child: Padding(
              padding: const EdgeInsets.all(6.0),
              child: ElevatedButton(
                child: Text(
                  '0',
                ),
                onPressed: () {
                  appendInputValue('0');
                },
                style: ElevatedButton.styleFrom(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(50),
                  ),
                ),
              ),
            ),
          ),
          Expanded(
            flex: 1,
            child: Padding(
              padding: const EdgeInsets.all(6.0),
              child: ElevatedButton(
                child: Text(','),
                onPressed: () {
                  appendInputValue(',');
                },
              ),
            ),
          ),
          Expanded(
            flex: 1,
            child: Padding(
              padding: const EdgeInsets.all(6.0),
              child: ElevatedButton(
                child: Text('='),
                onPressed: () {
                  calculate();
                },
                style: operatorStyle,
              ),
            ),
          ),
        ],
      ),
    );
  }

  Expanded FourthRow() {
    return Expanded(
      flex: 1,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Expanded(
            flex: 1,
            child: Padding(
              padding: const EdgeInsets.all(6.0),
              child: ElevatedButton(
                child: Text('1'),
                onPressed: () {
                  appendInputValue('1');
                },
              ),
            ),
          ),
          Expanded(
            flex: 1,
            child: Padding(
              padding: const EdgeInsets.all(6.0),
              child: ElevatedButton(
                child: Text('2'),
                onPressed: () {
                  appendInputValue('2');
                },
              ),
            ),
          ),
          Expanded(
            flex: 1,
            child: Padding(
              padding: const EdgeInsets.all(6.0),
              child: ElevatedButton(
                child: Text('3'),
                onPressed: () {
                  appendInputValue('3');
                },
              ),
            ),
          ),
          Expanded(
            flex: 1,
            child: Padding(
              padding: const EdgeInsets.all(6.0),
              child: ElevatedButton(
                child: Text('+'),
                onPressed: () {
                  _currentOperator = 'add';
                  _calculatedValue = _inputValue;
                  _inputValue = '0';
                  setState(() {});
                },
                style: operatorStyle,
              ),
            ),
          ),
        ],
      ),
    );
  }

  Expanded ThirdRow() {
    return Expanded(
      flex: 1,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Expanded(
            flex: 1,
            child: Padding(
              padding: const EdgeInsets.all(6.0),
              child: ElevatedButton(
                child: Text('4'),
                onPressed: () {
                  appendInputValue('4');
                },
              ),
            ),
          ),
          Expanded(
            flex: 1,
            child: Padding(
              padding: const EdgeInsets.all(6.0),
              child: ElevatedButton(
                child: Text('5'),
                onPressed: () {
                  appendInputValue('5');
                },
              ),
            ),
          ),
          Expanded(
            flex: 1,
            child: Padding(
              padding: const EdgeInsets.all(6.0),
              child: ElevatedButton(
                child: Text('6'),
                onPressed: () {
                  appendInputValue('6');
                },
              ),
            ),
          ),
          Expanded(
            flex: 1,
            child: Padding(
              padding: const EdgeInsets.all(6.0),
              child: ElevatedButton(
                child: Text('–'),
                onPressed: () {
                  _currentOperator = 'subtract';
                  _calculatedValue = _inputValue;
                  _inputValue = '0';
                  setState(() {});
                },
                style: operatorStyle,
              ),
            ),
          ),
        ],
      ),
    );
  }

  Expanded SecondRow() {
    return Expanded(
      flex: 1,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Expanded(
            flex: 1,
            child: Padding(
              padding: const EdgeInsets.all(6.0),
              child: ElevatedButton(
                child: Text('7'),
                onPressed: () {
                  appendInputValue('7');
                },
              ),
            ),
          ),
          Expanded(
            flex: 1,
            child: Padding(
              padding: const EdgeInsets.all(6.0),
              child: ElevatedButton(
                child: Text('8'),
                onPressed: () {
                  appendInputValue('8');
                },
              ),
            ),
          ),
          Expanded(
            flex: 1,
            child: Padding(
              padding: const EdgeInsets.all(6.0),
              child: ElevatedButton(
                child: Text('9'),
                onPressed: () {
                  appendInputValue('9');
                },
              ),
            ),
          ),
          Expanded(
            flex: 1,
            child: Padding(
              padding: const EdgeInsets.all(6.0),
              child: ElevatedButton(
                child: Text('×'),
                onPressed: () {
                  _currentOperator = 'multiply';
                  _calculatedValue = _inputValue;
                  _inputValue = '0';
                  setState(() {});
                },
                style: operatorStyle,
              ),
            ),
          ),
        ],
      ),
    );
  }

  Expanded FirstRow() {
    return Expanded(
      flex: 1,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Expanded(
            flex: 1,
            child: Padding(
              padding: const EdgeInsets.all(6.0),
              child: ElevatedButton(
                child: Text('AC'),
                style: secondaryStyle,
                onPressed: () {
                  if (_inputValue != '0') {
                    clearInputValue();
                  } else {
                    clearCalculatedValue();
                  }
                },
              ),
            ),
          ),
          Expanded(
            flex: 1,
            child: Padding(
              padding: const EdgeInsets.all(6.0),
              child: ElevatedButton(
                child: Text('±'),
                style: secondaryStyle,
                onPressed: () {},
              ),
            ),
          ),
          Expanded(
            flex: 1,
            child: Padding(
              padding: const EdgeInsets.all(6.0),
              child: ElevatedButton(
                child: Text('%'),
                style: secondaryStyle,
                onPressed: () {},
              ),
            ),
          ),
          Expanded(
            flex: 1,
            child: Padding(
              padding: const EdgeInsets.all(6.0),
              child: ElevatedButton(
                child: Text('÷'),
                onPressed: () {
                  _currentOperator = 'divide';
                  _calculatedValue = _inputValue;
                  _inputValue = '0';
                  setState(() {});
                },
                style: operatorStyle,
              ),
            ),
          ),
        ],
      ),
    );
  }

  void appendInputValue(String input) {
    if (_inputValue == '0') {
      _inputValue = input;
    } else if (_inputValue.length < 9) {
      _inputValue += input;
    }
    setState(() {});
  }

  void clearInputValue() {
    _inputValue = '0';
    setState(() {});
  }

  void clearCalculatedValue() {
    _calculatedValue = '';
    setState(() {
      
    });
  }

  void calculate() {
    num oldValue = num.tryParse(_calculatedValue) ?? 0.0;
    num inputValue = num.tryParse(_inputValue) ?? 0.0;
    num calculatedValue = 0.0;

    switch (_currentOperator) {
      case 'subtract':
        calculatedValue = oldValue - inputValue;
        break;

      case 'multiply':
        calculatedValue = oldValue * inputValue;
        break;

      case 'divide':
        try {
          calculatedValue = (oldValue / inputValue);
        } catch (e) {
          calculatedValue = 0.0;
        }
        break;

      case 'add':
        calculatedValue = oldValue + inputValue;
        break;

      default:
        calculatedValue = oldValue + inputValue;
        break;
    }

    _currentOperator = '';
    _calculatedValue = calculatedValue.toStringAsFixed(8);
    _inputValue = '0';
    setState(() {});
  }
}
