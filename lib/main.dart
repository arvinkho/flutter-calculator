import 'package:calculator_app/screens/calculator_screen.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const CalculatorApp());
}

class CalculatorApp extends StatelessWidget {
  const CalculatorApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primarySwatch: Colors.blue,
        elevatedButtonTheme: ElevatedButtonThemeData(
          style: ElevatedButton.styleFrom(
            primary: Color.fromARGB(255, 80, 80, 80),
            onPrimary: Colors.white,
            textStyle: TextStyle(
              fontSize: 36
            ),
            shape: CircleBorder(),            
          )
        )
      ),
      initialRoute: '/',
      routes: {
        '/': (context) => CalculatorScreen(),
      },
    );
  }
}
